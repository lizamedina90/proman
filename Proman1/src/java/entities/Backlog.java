/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "backlog")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Backlog.findAll", query = "SELECT b FROM Backlog b")
    , @NamedQuery(name = "Backlog.findByBacklogId", query = "SELECT b FROM Backlog b WHERE b.backlogId = :backlogId")
    , @NamedQuery(name = "Backlog.findByBacklogDesc", query = "SELECT b FROM Backlog b WHERE b.backlogDesc = :backlogDesc")})
public class Backlog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "backlog_id")
    private String backlogId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "backlog_desc")
    private String backlogDesc;
    @JoinColumn(name = "proyecto_id", referencedColumnName = "proyecto_id")
    @ManyToOne(optional = false)
    private Proyecto proyectoId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "backlogId")
    private Collection<Requerimiento> requerimientoCollection;

    public Backlog() {
    }

    public Backlog(String backlogId) {
        this.backlogId = backlogId;
    }

    public Backlog(String backlogId, String backlogDesc) {
        this.backlogId = backlogId;
        this.backlogDesc = backlogDesc;
    }

    public String getBacklogId() {
        return backlogId;
    }

    public void setBacklogId(String backlogId) {
        this.backlogId = backlogId;
    }

    public String getBacklogDesc() {
        return backlogDesc;
    }

    public void setBacklogDesc(String backlogDesc) {
        this.backlogDesc = backlogDesc;
    }

    public Proyecto getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(Proyecto proyectoId) {
        this.proyectoId = proyectoId;
    }

    @XmlTransient
    public Collection<Requerimiento> getRequerimientoCollection() {
        return requerimientoCollection;
    }

    public void setRequerimientoCollection(Collection<Requerimiento> requerimientoCollection) {
        this.requerimientoCollection = requerimientoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (backlogId != null ? backlogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Backlog)) {
            return false;
        }
        Backlog other = (Backlog) object;
        if ((this.backlogId == null && other.backlogId != null) || (this.backlogId != null && !this.backlogId.equals(other.backlogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Backlog[ backlogId=" + backlogId + " ]";
    }
    
}
