/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "rolusuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rolusuario.findAll", query = "SELECT r FROM Rolusuario r")
    , @NamedQuery(name = "Rolusuario.findByRolUsuarioid", query = "SELECT r FROM Rolusuario r WHERE r.rolUsuarioid = :rolUsuarioid")})
public class Rolusuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "rol_usuarioid")
    private Short rolUsuarioid;
    @JoinColumn(name = "proyecto_id", referencedColumnName = "proyecto_id")
    @ManyToOne(optional = false)
    private Proyecto proyectoId;
    @JoinColumn(name = "rol_id", referencedColumnName = "rol_id")
    @ManyToOne(optional = false)
    private Rol rolId;
    @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id")
    @ManyToOne(optional = false)
    private Usuario usuarioId;

    public Rolusuario() {
    }

    public Rolusuario(Short rolUsuarioid) {
        this.rolUsuarioid = rolUsuarioid;
    }

    public Short getRolUsuarioid() {
        return rolUsuarioid;
    }

    public void setRolUsuarioid(Short rolUsuarioid) {
        this.rolUsuarioid = rolUsuarioid;
    }

    public Proyecto getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(Proyecto proyectoId) {
        this.proyectoId = proyectoId;
    }

    public Rol getRolId() {
        return rolId;
    }

    public void setRolId(Rol rolId) {
        this.rolId = rolId;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rolUsuarioid != null ? rolUsuarioid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rolusuario)) {
            return false;
        }
        Rolusuario other = (Rolusuario) object;
        if ((this.rolUsuarioid == null && other.rolUsuarioid != null) || (this.rolUsuarioid != null && !this.rolUsuarioid.equals(other.rolUsuarioid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Rolusuario[ rolUsuarioid=" + rolUsuarioid + " ]";
    }
    
}
