/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "requerimiento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Requerimiento.findAll", query = "SELECT r FROM Requerimiento r")
    , @NamedQuery(name = "Requerimiento.findByRequerimientoId", query = "SELECT r FROM Requerimiento r WHERE r.requerimientoId = :requerimientoId")
    , @NamedQuery(name = "Requerimiento.findByRequerimientoDesc", query = "SELECT r FROM Requerimiento r WHERE r.requerimientoDesc = :requerimientoDesc")})
public class Requerimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "requerimiento_id")
    private Short requerimientoId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "requerimiento_desc")
    private String requerimientoDesc;
    @JoinColumn(name = "backlog_id", referencedColumnName = "backlog_id")
    @ManyToOne(optional = false)
    private Backlog backlogId;

    public Requerimiento() {
    }

    public Requerimiento(Short requerimientoId) {
        this.requerimientoId = requerimientoId;
    }

    public Requerimiento(Short requerimientoId, String requerimientoDesc) {
        this.requerimientoId = requerimientoId;
        this.requerimientoDesc = requerimientoDesc;
    }

    public Short getRequerimientoId() {
        return requerimientoId;
    }

    public void setRequerimientoId(Short requerimientoId) {
        this.requerimientoId = requerimientoId;
    }

    public String getRequerimientoDesc() {
        return requerimientoDesc;
    }

    public void setRequerimientoDesc(String requerimientoDesc) {
        this.requerimientoDesc = requerimientoDesc;
    }

    public Backlog getBacklogId() {
        return backlogId;
    }

    public void setBacklogId(Backlog backlogId) {
        this.backlogId = backlogId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (requerimientoId != null ? requerimientoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Requerimiento)) {
            return false;
        }
        Requerimiento other = (Requerimiento) object;
        if ((this.requerimientoId == null && other.requerimientoId != null) || (this.requerimientoId != null && !this.requerimientoId.equals(other.requerimientoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Requerimiento[ requerimientoId=" + requerimientoId + " ]";
    }
    
}
