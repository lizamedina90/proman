/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "userstory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Userstory.findAll", query = "SELECT u FROM Userstory u")
    , @NamedQuery(name = "Userstory.findByUserstoryId", query = "SELECT u FROM Userstory u WHERE u.userstoryId = :userstoryId")
    , @NamedQuery(name = "Userstory.findByUserstoryEstado", query = "SELECT u FROM Userstory u WHERE u.userstoryEstado = :userstoryEstado")})
public class Userstory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userstory_id")
    private Short userstoryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "userstory_estado")
    private String userstoryEstado;
    @JoinColumn(name = "sprint_id", referencedColumnName = "sprint_id")
    @ManyToOne(optional = false)
    private Sprint sprintId;
    @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id")
    @ManyToOne(optional = false)
    private Usuario usuarioId;

    public Userstory() {
    }

    public Userstory(Short userstoryId) {
        this.userstoryId = userstoryId;
    }

    public Userstory(Short userstoryId, String userstoryEstado) {
        this.userstoryId = userstoryId;
        this.userstoryEstado = userstoryEstado;
    }

    public Short getUserstoryId() {
        return userstoryId;
    }

    public void setUserstoryId(Short userstoryId) {
        this.userstoryId = userstoryId;
    }

    public String getUserstoryEstado() {
        return userstoryEstado;
    }

    public void setUserstoryEstado(String userstoryEstado) {
        this.userstoryEstado = userstoryEstado;
    }

    public Sprint getSprintId() {
        return sprintId;
    }

    public void setSprintId(Sprint sprintId) {
        this.sprintId = sprintId;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userstoryId != null ? userstoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Userstory)) {
            return false;
        }
        Userstory other = (Userstory) object;
        if ((this.userstoryId == null && other.userstoryId != null) || (this.userstoryId != null && !this.userstoryId.equals(other.userstoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Userstory[ userstoryId=" + userstoryId + " ]";
    }
    
}
