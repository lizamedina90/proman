/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Andrea
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByUsuarioId", query = "SELECT u FROM Usuario u WHERE u.usuarioId = :usuarioId")
    , @NamedQuery(name = "Usuario.findByUsuarioNombre", query = "SELECT u FROM Usuario u WHERE u.usuarioNombre = :usuarioNombre")
    , @NamedQuery(name = "Usuario.findByUsuarioCorreo", query = "SELECT u FROM Usuario u WHERE u.usuarioCorreo = :usuarioCorreo")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "usuario_id")
    private Short usuarioId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuario_nombre")
    private short usuarioNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuario_correo")
    private short usuarioCorreo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuarioId")
    private Collection<Rolusuario> rolusuarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuarioId")
    private Collection<Userstory> userstoryCollection;

    public Usuario() {
    }

    public Usuario(Short usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Usuario(Short usuarioId, short usuarioNombre, short usuarioCorreo) {
        this.usuarioId = usuarioId;
        this.usuarioNombre = usuarioNombre;
        this.usuarioCorreo = usuarioCorreo;
    }

    public Short getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Short usuarioId) {
        this.usuarioId = usuarioId;
    }

    public short getUsuarioNombre() {
        return usuarioNombre;
    }

    public void setUsuarioNombre(short usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    public short getUsuarioCorreo() {
        return usuarioCorreo;
    }

    public void setUsuarioCorreo(short usuarioCorreo) {
        this.usuarioCorreo = usuarioCorreo;
    }

    @XmlTransient
    public Collection<Rolusuario> getRolusuarioCollection() {
        return rolusuarioCollection;
    }

    public void setRolusuarioCollection(Collection<Rolusuario> rolusuarioCollection) {
        this.rolusuarioCollection = rolusuarioCollection;
    }

    @XmlTransient
    public Collection<Userstory> getUserstoryCollection() {
        return userstoryCollection;
    }

    public void setUserstoryCollection(Collection<Userstory> userstoryCollection) {
        this.userstoryCollection = userstoryCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioId != null ? usuarioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuarioId == null && other.usuarioId != null) || (this.usuarioId != null && !this.usuarioId.equals(other.usuarioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Usuario[ usuarioId=" + usuarioId + " ]";
    }
    
}
